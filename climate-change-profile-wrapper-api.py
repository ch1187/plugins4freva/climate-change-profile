# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause

"""
Creating Climate Change Profiles plugin API wrapper.
"""

import json
import os
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple

import freva
from evaluation_system.api import plugin
from evaluation_system.api.parameters import Bool, File, Float, Integer
from evaluation_system.api.parameters import ParameterDictionary as ParamDict
from evaluation_system.api.parameters import SelectField, SolrField
from evaluation_system.misc import config, logger
from setup_info import FIVE_BY_FIVE, MUNICIPALITIES


class ClimateChangeProfile(plugin.PluginAbstract):
    """Create climate change profiles."""

    tool_developer = {"name": "Martin Bergemann", "email": "bergemann@dkrz.de"}
    __category__ = "visualisation"
    __short_description__ = "Create climate change signals."
    __long_description__ = """This plug-in calculates the difference between
a future time period and a historical time period of climate model simulations
for regions and variables of interest. The results include maps, time series
and downloadable data sets."""
    __version__ = (2022, 7, 16)
    __parameters__ = ParamDict(
        File(
            name="shape_file",
            file_extension="geojson",
            help=(
                "Enter a geo reference data file defining "
                "your region of interest. You can either enter a path on the "
                "HPC system or a web url pointing to the geo reference data."
                "If you don't have a geo reference data file, select a region "
                " in the select menu below (Region name)."
            ),
            default=None,
        ),
        SelectField(
            name="region_name",
            default="Germany",
            options={mun: mun for mun in ["Germany"] + sorted(MUNICIPALITIES)},
            help=(
                "Select your region of interest fom a list of German "
                "municipalities if you have not entered a geo reference data file"
                " (shape file). This selection has only effect if you "
                "don't chose a shape file Selecting 'Germany' will use all "
                "municipalities."
            ),
        ),
        Integer(
            name="begin_historical",
            default=1970,
            help=(
                "Set the start year of the historical reference "
                "period. Keep in mind that some model simulations cannot "
                "be included in the calculation of the climate change "
                "signal if you choose an earlier start year than 1970."
            ),
        ),
        Float(
            name="global_warming_level",
            default=2.0,
            help=(
                "Set the global warming level in degrees Celsius. This is the "
                "future scenario equivalent of the `begin_historical` key. "
                "The only difference is that you don't have to choose a year "
                "as the future scenario is based on a global warming level. "
                "This way all models will be based on the same global warming "
                "signal, while the actual year when a given warming signal "
                "occurs varies."
            ),
        ),
        Integer(
            name="num_years",
            mandatory=False,
            default=30,
            help=(
                "The plugin calculates spatio-temporal averages. Select the "
                "number of years the averaging process is based on."
            ),
        ),
        SolrField(
            name="product",
            mandatory=True,
            facet="product",
            default="eur-11",
            help=(
                "Define your regional climate model ensemble: Select the "
                "climate model parent product."
            ),
            max_items=1,
            predefined_facets={
                "project": ["cordex", "nukleus", "cordex-nukleus"]
            },
        ),
        SolrField(
            name="scenario",
            mandatory=True,
            facet="experiment",
            help=(
                "Define your regional climate model ensemble: Select the "
                "climate change scenario."
            ),
            max_items=1,
            default="rcp85",
            predefined_facets=dict(
                project=["cordex", "nukleus", "cordex-nukleus"],
                experiment=["rcp26", "rcp85", "rcp45"],
            ),
        ),
        SolrField(
            name="models",
            facet="model",
            multiple=True,
            default=None,
            max_items=100,
            predefined_facets=dict(
                project=["cordex", "nukleus", "cordex-nukleus"]
            ),
            help=(
                "Define your regional climate model ensemble: Select "
                "(multiple) climate models. A larger ensemble can improve "
                "the robustness of the results. If you leave this field empty,"
                " an ensemble with 25 selected members will be taken. "
                " This ensemble is designed by nukleus to "
                "represent Germany's climate as best as possible following "
                "specific criteria."
            ),
        ),
        SolrField(
            name="variable",
            mandatory=True,
            facet="variable",
            multiple=True,
            default=("pr", "tas"),
            max_items=100,
            help=(
                "Select the variable name(s) of interest. Precipitation and "
                "surface temperature is selected by default, but you can choose"
                " from the full range of available variables."
            ),
        ),
        SelectField(
            name="mask_method",
            default="centres",
            options={"centres": "centres", "corners": "corners"},
            help=(
                "Select the method how your region of interest is masked:"
                "`centres` selects points where only grid-cell centres are "
                "within the polygon of the region. `corners` selects "
                "grid-cells where any of the 4 grid-cell corners lie within "
                "the polygon of the shape region. Select `corners` if you want"
                " to be the region selection less restrictive."
            ),
        ),
        Bool(
            name="abort_on_errors",
            default=False,
            help=(
                "Set this switch to True to abort the plugin application "
                "if data for a certain ensemble member can't be read "
                "(because of corrupted files). Otherwise a member with "
                "too many corrupted files will be skipped (default)."
            ),
        ),
        Bool(
            name="use_dask",
            default=True,
            help=(
                "Make use of dask distributed computing resources for faster "
                "data processing. Setting this flag to 'False' might help with"
                " debugging unexpected results."
            ),
        ),
    )

    @staticmethod
    def _drs_search(**search_kw: str) -> Optional[list[str]]:
        kw = search_kw.copy()
        for time_frequency in ("day", "6hr", "3hr", "1hr"):
            kw["time_frequency"] = time_frequency
            files = sorted(freva.databrowser(**kw))
            if files:
                return files
        return None

    def _get_files(
        self,
        variable: str,
        experiment: str,
        model: str,
        product: str,
        projects: list[str],
        prefered: str = "r1i1p1",
        start_year: Optional[int] = None,
        years: int = 30,
    ) -> Dict[Any, Tuple[List[str], int]]:
        files = {}
        search_kw = dict(variable=variable, experiment=experiment, model=model)
        if start_year:
            search_kw["time"] = f"{start_year}-01-31TO{start_year+years}-12-31"

        if product:
            search_kw["product"] = product
        experiment_facets = freva.facet_search(**search_kw, facet=None)
        ensembles = sorted(ens for ens in experiment_facets["ensemble"])

        if prefered in ensembles:
            ensembles = [prefered]
        if ensembles:
            ens = ensembles[0]
            for project in projects:
                search_kw["project"] = project
                search_kw["ensemble"] = ens
                files_found = self._drs_search(**search_kw)
                if not files_found:
                    continue
                if start_year is not None:
                    files[ens] = files_found, start_year
                    break
                cmip_kwargs = self._get_cmip(**search_kw)
                if cmip_kwargs is not None:
                    files[ens] = files_found, cmip_kwargs
                    break
        return files

    def _get_cmip(self, **search_kwargs):
        kwargs = search_kwargs.copy()
        _ = kwargs.pop("time", "")
        model = kwargs.pop("model")
        proj = None
        for project in ("cmip5", "cmip6"):
            models = freva.facet_search(
                project=project,
                variable="tas",
                experiment=kwargs["experiment"],
                ensemble=kwargs["ensemble"],
                facet="model",
            )["model"]
            if models:
                proj = project
                models = models[::2]
                break
        for m in models:
            if m in model:
                dd = dict(
                    project=proj,
                    model=m,
                    experiment=kwargs["experiment"],
                    ensemble=kwargs["ensemble"],
                )
                return dd

    def _get_model(self, models, target):
        for model in models:
            if model.lower() in target.lower():
                return model

    def _get_warming_levels(self, level, keys):
        import time
        from subprocess import PIPE, Popen

        if level < 0:
            logger.warning(
                "Negative warming levels might lead to unexpected results"
            )
        pool = {}
        logger.info(
            "Calculating global warming levels for %s K warming", level
        )
        for nn, key in enumerate(keys):
            proj, model, exp, ens = key.split(".")
            cmd = [
                "gwl-loader",
                "get-year",
                f"{level}",
                model,
                "--experiment",
                exp,
                "--ensemble",
                ens,
                "--project",
                proj,
                "--method",
                "freva",
            ]
            print(" ".join(cmd))
            pool[key] = Popen(
                cmd, stdout=PIPE, stderr=PIPE, universal_newlines=True
            )
            if (nn + 1) % 4 == 0:
                while (
                    sum([1 if p.poll() is None else 0 for p in pool.values()])
                    > 0
                ):
                    time.sleep(0.5)
        for key, res in pool.items():
            result = res.communicate()
            try:
                res = (
                    result[0].partition(":")[-1].strip().split("-")[0].strip()
                )
                pool[key] = "%04i-01-01" % int(res)
            except (ValueError, AttributeError):
                pool[key] = None
        return pool

    def run_tool(self, config_dict):
        out_dir = self._special_variables.substitute({"d": "$USER_OUTPUT_DIR"})
        out_dir = Path(out_dir["d"]) / str(self.rowid)
        out_dir.mkdir(exist_ok=True, parents=True)
        comp = config.get_section("scheduler_options").get("project", "")
        config_dict["account"] = comp or "ch1187"
        config_dict["output_dir"] = str(out_dir)
        Path(config_dict["output_dir"]).mkdir(exist_ok=True, parents=True)
        exps = dict(
            hist="historical", proj=config_dict.pop("scenario", "rcp85")
        )
        models = sorted(
            config_dict.pop("models", FIVE_BY_FIVE) or FIVE_BY_FIVE
        )
        product = config_dict.get("product", None)
        config_dict["region"] = ""
        if (config_dict["region_name"] or "").lower() == "germany":
            config_dict["region_name"] = None
        else:
            for num, mun in enumerate(MUNICIPALITIES):
                if mun.lower() == config_dict["region_name"].lower():
                    config_dict["region_name"] = str(num)
                    break
        files = {}
        search_kw = dict(
            model=models,
            experiment=[exps["hist"], exps["proj"]],
            variable=config_dict["variable"],
            product=product,
        )
        config_dict["projects"] = [
            project
            for project in freva.facet_search(**search_kw, facet="project")[
                "project"
            ]
            if project
        ]
        config_dict["experiments"] = exps
        if isinstance(models, str):
            models = [models]
        for exp, name in exps.items():
            if exp == "hist":
                start_year = int(config_dict["begin_historical"])
            else:
                start_year = None
            files[exp] = {
                m: self._get_files(
                    config_dict["variable"],
                    name,
                    m,
                    product,
                    config_dict["projects"],
                    start_year=start_year,
                    years=config_dict["num_years"],
                )
                for m in models
            }
        cfg, models = [], {}
        for model, var in files["proj"].items():
            for ens, (ff, kw) in var.items():
                if kw is None:
                    continue
                cfg.append(
                    "{}.{}.{}.{}".format(
                        kw["project"],
                        kw["model"],
                        kw["experiment"],
                        kw["ensemble"],
                    )
                )
                try:
                    models[kw["model"]].append((model, kw["ensemble"]))
                except KeyError:
                    models[kw["model"]] = [model, kw["ensemble"]]
        if config_dict["num_years"] < 1:
            logger.error("The number of years to average should be > 1")
            raise ValueError("The number of years to average should be > 1")
        gwl = self._get_warming_levels(
            config_dict["global_warming_level"], set(cfg)
        )
        delete = []
        for model, var in files["proj"].items():
            for ens, (fs, kw) in var.items():
                key = "{}.{}.{}.{}".format(
                    kw["project"],
                    kw["model"],
                    kw["experiment"],
                    kw["ensemble"],
                )
                year = gwl[key]
                if year is None:
                    logger.warning(f"No warming level found for {model} {ens}")
                    delete.append((model, ens))
                else:
                    first_year = int(year.split("-")[0])
                    last_year = first_year + config_dict["num_years"]
                    kwargs = {
                        k: v for (k, v) in search_kw.items() if k != "model"
                    }
                    num_files = freva.count_values(
                        time=f"{last_year}",
                        model=model,
                        ensemble=ens,
                        **kwargs,
                    )
                    if num_files:
                        files["proj"][model][ens] = fs, year
                    else:
                        logger.warning(
                            "No files found that cover the requested"
                            " time period for the given warming level"
                            " and model {model} {ens}"
                        )
                        delete.append((model, ens))

        for model, ens in delete:
            try:
                files["proj"][model].pop(ens)
                files["hist"][model].pop(ens)
            except KeyError:
                pass
            if not files["proj"][model]:
                files["proj"].pop(model)
                files["hist"].pop(model)
        if not config_dict["shape_file"]:
            config_dict["shape_file"] = str(
                Path(__file__).parent / "assets" / "GER.shp"
            )
        else:
            # We don't need a region name
            config_dict["region_name"] = None
        if not files["proj"] or not files["hist"]:
            logger.error("Setup did not yield any data files.")
            raise ValueError("Setup did not yield any data files.")
        config_dict["files"] = files
        python_env = Path(__file__).parent / "plugin_env" / "bin" / "python"
        proj_db = Path(__file__).parent / "plugin_env" / "share" / "proj"
        tool_path = Path(__file__).parent / "assets" / "run.py"
        config_file = (
            Path(config_dict["output_dir"]) / "climate_change_profile.json"
        )
        config_dict["write"] = True
        with open(str(config_file), "w") as f:
            json.dump(config_dict, f, indent=4)
        cmd = "{} -B {} {}".format(python_env, tool_path, config_file)
        env = os.environ.copy()
        env["PROJ_LIB"] = str(proj_db.absolute())
        self.call(cmd, env=env)
        for path in ("dask-worker-space", "__pycache__"):
            rmpath = Path(config_dict["output_dir"]) / path
            if rmpath.is_dir():
                os.system("rm -r %s" % str(rmpath))
        return self.prepare_output(config_dict["output_dir"])

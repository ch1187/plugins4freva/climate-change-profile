#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: CC0-1.0

import versioneer
from setuptools import find_packages, setup

setup(
    version=versioneer.get_version(),
    packages=find_packages("src"),
    package_dir={"": "src"},
)

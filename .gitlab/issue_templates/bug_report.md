<!--
SPDX-FileCopyrightText: 2024 Deutsches Klimarechenzentrum GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

## Description

Please provide a clear and concise description of the issue.

## Configuration Used
```bash
freva-plugin <PLUGIN_NAME> <ARGS>
```

### Relevant Logs and Files

- **Log URL**: [Job Information](https://link/of/job/log/in/freva/wb)
- **Jupyter Notebook**: `/jupyter/notebook/location/address/in/levante`
- **Other Details**: Attached file or somethig else

### Observed Error

```bash
# Insert the observed error message here, including file paths and line numbers if available.
```

## Steps to Reproduce

1. Step one
2. Step two
3. Step three

### Possible Cause

Describe the possible cause for getting this bug.
## Expected Behavior

Describe what you expected to happen.

## Additional Information

Add any other context about the problem here.

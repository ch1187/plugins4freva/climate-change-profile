# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause

import json
import logging

# import shlex
import traceback
from pathlib import Path

# from subprocess import PIPE, run
from tempfile import TemporaryDirectory

# import geopandas as gp
import papermill as pm
from ipykernel.kernelspec import install as install_kernel

from ccp import Ensemble  # noqa


def parse_config(argv=None):
    """Construct command line argument parser."""
    import argparse

    argp = argparse.ArgumentParser
    ap = argp(
        prog="climate_change_profiler",
        description="Plot some maps of different climatechange profiles",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    ap.add_argument(
        "configfile",
        metavar="configfile",
        type=Path,
        help="The configuration file.",
    )
    args = ap.parse_args()
    config_file = args.configfile.expanduser().absolute()
    with config_file.open() as f:
        return config_file, json.load(f)


def install_jupyter_kernel(kernel_name="freva-ccp"):
    kernel_dir = Path("~/.local/share/jupyter/kernels").expanduser()
    if not (kernel_dir / kernel_name).is_dir():
        install_kernel(
            user=True, kernel_name=kernel_name, display_name=kernel_name
        )


def main(config_file, cfg):
    install_jupyter_kernel()
    logging.basicConfig(
        level=logging.ERROR, format="%(name)s;%(levelname)s;%(message)s"
    )
    print("Executing ClimateChangeProfile notebook")
    pm.log.logger.setLevel(logging.INFO)
    logger = logging.getLogger("ClimateChangeProfile")  # noqa
    out_dir = Path(cfg["output_dir"])
    this_path = Path(__file__).parent
    nb_out = out_dir / "ClimateChangeProfile.ipynb"
    nb_in = this_path / "ClimateChangeProfile.ipynb"
    with config_file.open("w") as f:
        json.dump(cfg, f)
    with TemporaryDirectory() as td:  # noqa
        try:
            pm.execute_notebook(
                str(nb_in),
                str(nb_out),
                cwd=str(out_dir),
                parameters={
                    "config_file": str(config_file),
                    "path": str(out_dir),
                },
                progress_bar=False,
                kernel_name="freva-ccp",
                log_output=True,
                prepare_only=False,
                stdout_file=sys.stdout,
                stderr_file=sys.stderr,
            )
            success = True
        except Exception as error:
            traceback.print_exception(error, limit=1)
            success = False
    if success is False:
        raise ValueError(
            f"Notebook execution failed. Find the notebook in: {nb_out}"
        )
    _ = cfg.pop("write", None)
    with config_file.open("w") as f:
        json.dump(cfg, f)
    for file in out_dir.rglob("*"):
        file.chmod(0o755)
    out_dir.chmod(0o755)


if __name__ == "__main__":
    import sys

    import dask
    from distributed.utils import logger as dask_logger

    with dask.config.set({"logging.distributed": "critical"}):
        dask_logger.setLevel(logging.ERROR)
        cfg = main(*parse_config(sys.argv))

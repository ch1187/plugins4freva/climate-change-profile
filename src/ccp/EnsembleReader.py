# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause

from __future__ import annotations

import json
import logging
import os
import zipfile
from contextlib import contextmanager
from datetime import datetime
from functools import cached_property, partial
from getpass import getuser
from itertools import product as it_product
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Any, Dict, Iterator, Union, cast

import cartopy.feature as cfeature
import dask
import geopandas as gp
import humanize
import matplotlib.dates as mdates
import numpy as np
import pandas as pd
import pyproj
import seaborn as sns
import xarray as xr
import xarray.plot as xr_plot
from cartopy import crs
from cartopy.feature import ShapelyFeature
from climpact import Averager, RunDirectory, UnitConverter
from dask.distributed.client import Client
from dask_jobqueue import SLURMCluster
from fiona.errors import DriverError, SchemaError
from IPython.core.display import HTML
from matplotlib import pyplot as plt
from matplotlib.figure import Figure

from .PlotFactory import AuxInfo

logger = logging.getLogger("ClimateChangeProfile")

# Timedelta in years, how long is an average period (30 years)
DT = 30

# The Quantile definition defining the ensemble boundaries
QUANTILES = [0.2, 0.5, 0.8]


class Ensemble(AuxInfo):
    """Class that defines and creates a multi model ensemble."""

    earth_radius = 6370000
    transform = crs.PlateCarree()
    glb = True
    rot_pole = None

    @staticmethod
    def _read_data(
        inp_data: tuple[list[str], int],
        variables: list[str],
        dt: int = 30,
        abort_on_error: bool = False,
        shape_file: str = "",
        region_name: str = "",
        exp: str = "",
        model: str = "",
    ) -> xr.Dataset | None:
        """Read and mask a dataset."""
        inp_files, start_year = inp_data
        start_year = int(str(start_year).partition("-")[0])
        start = f"{start_year}-01-01"
        end = f"{start_year + dt}-12-30"
        rd = RunDirectory(
            sorted(inp_files),
            variables,
            abort_on_error=abort_on_error,
            shape_file=shape_file,
            region=region_name,
            model=model,
            start=start,
            end=end,
            parallel=True,
            exp=exp,
        )
        abort_on_error = True
        if rd._dataset is None:
            return None
        try:
            dataset = (
                rd.dataset[rd.variables]  # type: ignore
                .groupby("time.dayofyear")
                .mean(dim="time", keep_attrs=True)
                .rename({"dayofyear": "time"})
                .isel(time=slice(0, 360))
                .assign_coords(
                    {"time": pd.date_range("2020-01-01", periods=360)}
                )
                .resample(time="7D")
                .mean("time", keep_attrs=True)
            )
            squeeze = [d for d, l in dataset.dims.items() if l == 1]
            dataset._coord_names.discard(("X", "Y"))
            dataset = xr.concat(
                [dataset.squeeze(squeeze)], dim="member"
            ).assign_coords({"member": [model]})
            dataset["start"] = xr.DataArray(
                [start_year], dims=("member",), coords={"member": [model]}
            )
            dataset["end"] = xr.DataArray(
                [start_year + dt], dims=("member",), coords={"member": [model]}
            )
            dataset["mask"] = xr.DataArray(
                rd.mask.data, dims=dataset[rd.variables[0]].dims[-2:]
            )
            dataset.attrs["experiment"] = exp
            return dataset
        except Exception as error:
            if abort_on_error:
                raise error
            print(f"Warning, failed to process {model} / {exp}: {error}")
        return None

    @staticmethod
    def _create_ensemble(
        datasets: dict[str, xr.Dataset], member: list[str]
    ) -> xr.Dataset:
        """Combine dataset to one ensemble."""
        if not member:
            raise ValueError("No member found, cannot proceed analysing data")
        variables = [
            v for v in datasets[member[0]] if v not in ("mask", "Y", "X")
        ]
        dims = {
            c: datasets[member[0]][c]
            for c in datasets[member[0]].dims.keys()
            if c not in ("member",)
        }
        mask = datasets[member[0]].mask
        merge_datasets = [datasets[member[0]][variables]]
        for mem in member[1:]:
            try:
                dset = datasets[mem][variables].reindex(dims)
            except KeyError:
                continue
            drop_coords = [
                c
                for c in dset.coords.keys()
                if c not in dset.dims and c not in ("mask", "Y", "X")
            ]
            dset["Y"] = merge_datasets[0]["Y"]
            dset["X"] = merge_datasets[0]["X"]
            merge_datasets.append(dset.drop(drop_coords))
        data = xr.concat(merge_datasets, dim="member", coords="minimal")
        data["mask"] = mask
        for var in variables:
            try:
                data[var] *= mask.data
            except Exception as error:
                print(f"Failed to mask {var}: {error}")
                pass
        return data

    @contextmanager
    @staticmethod
    def create_dask_cluster(
        account: str,
        workers: int,
        queue: str = "compute",
        memory: str = "256GiB",
        interface: str = "ib0",
        cores: int = 256,
        use_dask: bool = True,
    ) -> Iterator[Client | None]:
        """Initialise a dask distributed cluster for faster data processing.

        This creates a cluster based on dask-jobqueue's SLURMCluster,
        meaning that slurm jobs will be submitted in order to execute
        any calculation on those slurm nodes.

        Parameters
        -----------
        account: str
            Slurm account that is used to submit the jobs
        workders: int,
            Size of the cluster (number of workers).
        qeueue: str, default: compute
            The name of the queue to submit the jobs to.
        interface: str, default: ib0
            The name of the interface used for network
            communication
        memory: str, default: 256GiB
            Memory per submitted job
        cores: int, default: 256
            CPU cores per submitted job
        use_dask: bool, default: True
            debug mode, do not create a cluster.

        Returns
        --------
        dask.distributed.client:
            Instance of a dask distributed client used for processing.
        """
        old_scheduler = dask.config.get("scheduler", None)
        scratch = os.getenv(
            "SCRATCH_DIR", f"/scratch/{getuser()[0]}/{getuser()}"
        )
        temp_dir = TemporaryDirectory(dir=scratch)
        try:
            if use_dask is False:
                da_client = None
            elif old_scheduler is None:
                cluster = SLURMCluster(
                    account=account,
                    queue=queue,
                    memory=memory,
                    interface=interface,
                    cores=cores,
                    local_directory=temp_dir.name,
                )
                cluster.scale(n=int(1.5 * workers))
                da_client = Client(cluster)
                dask.config.set(scheduler=da_client)
                logger.info(
                    "Scaling distributed client to %s workers", workers
                )
                da_client.wait_for_workers(workers)
            else:
                da_client = old_scheduler
            yield da_client
        finally:
            dask.config.set(scheduler=old_scheduler)
            if old_scheduler is None and use_dask and da_client is not None:
                da_client.close()
                if (
                    hasattr(da_client, "cluster")
                    and da_client.cluster is not None
                ):
                    da_client.cluster.close()
            temp_dir.cleanup()
            for f in Path(temp_dir.name).rglob("*"):
                f.unlink()
            # _ = [f.unlink() for f in Path.cwd().rglob("slurm*.out")]

    @staticmethod
    def _get_files_from_key(
        file_config: dict[str, dict[str, tuple[list[str], int]]]
    ) -> dict[tuple[str, str], tuple[list[str], int]]:
        out = {}
        for model in sorted(file_config.keys()):
            for ens, (files, year) in file_config[model].items():
                out[(model, ens)] = (
                    sorted(files),
                    int(str(year).partition("-")[0]),
                )
        return out

    @classmethod
    def from_dict(cls, config_file: str | Path):
        """Create an instance of the ensemble class for inforamtion stored in a dict.

        Parameters
        -----------
        config_file: str
            Path to the dictionary holding the information.
        """
        with open(config_file) as f_obj:
            cfg = json.load(f_obj)

        files_hist: dict[
            tuple[str, str], tuple[list[str], int]
        ] = cls._get_files_from_key(cfg["files"]["hist"])
        files_proj: dict[
            tuple[str, str], tuple[list[str], int]
        ] = cls._get_files_from_key(cfg["files"]["proj"])
        filesizes = 0
        for file_dict in (files_hist, files_proj):
            for files, _ in file_dict.values():
                for f_path in files:
                    try:
                        filesizes += os.path.getsize(f_path)
                    except Exception as error:
                        print(f"Failed to get file size: {error}")
                        pass
        proc_sizes = humanize.filesize.naturalsize(filesizes)
        cfg["variable"].sort()
        if cfg.get("use_dask", True):
            _read_data = dask.delayed(cls._read_data)
        else:
            _read_data = cls._read_data
        with cls.create_dask_cluster(
            cfg.get("account", "ch1187"),
            max(len(files_hist), len(files_proj)),
            queue=cfg.get("queue", "compute"),
            memory=cfg.get("memory", "256GiB"),
            use_dask=cfg.get("use_dask", True),
        ) as dask_client:
            start_time = datetime.now()
            read_func = partial(
                _read_data,
                dt=cfg["num_years"],
                variables=cfg["variable"],
                abort_on_error=cfg["abort_on_errors"],
                shape_file=cfg["shape_file"],
                region_name=cfg["region_name"],
            )
            hist_future = {
                f"{k[0]}_{k[-1]}": read_func(
                    f,
                    model=f"{k[0]}_{k[-1]}",
                    exp=cfg["experiments"]["hist"],
                )
                for k, f in files_hist.items()
            }
            proj_future = {
                f"{k[0]}_{k[-1]}": read_func(
                    f,
                    model=f"{k[0]}_{k[-1]}",
                    exp=cfg["experiments"]["proj"],
                )
                for k, f in files_proj.items()
            }
            if cfg.get("use_dask", True):
                dset_hist, dset_proj = dask.compute(
                    hist_future, proj_future, scheduler=dask_client
                )
            else:
                dset_hist, dset_proj = hist_future, proj_future

            member = set(
                [k for k, v in dset_hist.items() if v is not None]
            ) & set([k for k, v in dset_proj.items() if v is not None])
            dset_hist = {k: dset_hist[k] for k in member}
            dset_proj = {k: dset_proj[k] for k in member}
            for m in member:
                dset_hist[m] = dset_hist[m].load(scheduler=dask_client)
                dset_proj[m] = dset_proj[m].load(scheduler=dask_client)
            hist = cls._create_ensemble(dset_hist, list(member)).load()
            proj = cls._create_ensemble(dset_proj, list(member)).load()
            for key in ("region_name", "projects", "product", "num_years"):
                if isinstance(cfg[key], list):
                    value = ",".join(cfg[key])
                else:
                    value = cfg[key]
                proj.attrs[key] = hist.attrs[key] = value
            proj.attrs["warming_level"] = cfg["global_warming_level"]
            time_delta = (datetime.now() - start_time).total_seconds()
            time_delta_h = humanize.time.naturaldelta(time_delta)
            print(f"Processed {proc_sizes} within {time_delta_h}")
            return cls(
                cfg["variable"],
                hist,
                proj,
                cfg["shape_file"],
                output_dir=cfg["output_dir"],
                write=cfg.get("write", False),
            )

    @cached_property
    def proj(self) -> crs.Projection:
        if "grid_north_pole_longitude" in self.projection_data.attrs:
            proj = crs.RotatedPole(
                self.projection_data.attrs["grid_north_pole_longitude"],
                self.projection_data.attrs["grid_north_pole_latitude"],
            )
        else:
            proj = crs.PlateCarree()
        return proj

    @staticmethod
    def _get_ts_data(
        dset: xr.Dataset, dim: str = "time"
    ) -> list[tuple[float, float]]:
        """Create a time series from a given dataset."""
        data = []
        for attr in ("min", "mean", "max"):
            array = getattr(dset, attr)(dim=dim)
            array = array.load()
            data.append(
                (
                    float(array.median(dim="member").values),
                    float(array.std(dim="member").values),
                )
            )
        return data

    def _create_information(self, dset: xr.Dataset):
        """Calculate spatio-temporal means/min/max values of a given dataset.

        Those mean/min/max values are stored in a dictionary.
        """
        info = {}
        seas = {
            "DJF": ("Dec", "Jan", "Feb"),
            "MAM": ("Mar", "Apr", "May"),
            "JJA": ("Jun", "Jul", "Aug"),
            "SON": ("Sep", "Oct", "Nov"),
        }
        for var in dset.data_vars:
            mon_data = {}
            coords = dset[var].dims[-2:]
            ts = dset[var].mean(dim=coords)
            dd = ts.resample(time="m")
            mm = dd.mean()
            dd_list = list(dd)
            months = [
                tt.strftime("%b") for tt in pd.DatetimeIndex(mm.time.values)
            ]
            for seas_name, mon in seas.items():
                idx = xr.concat(
                    [
                        dd_list[i][-1]
                        for i in range(len(months))
                        if months[i] in mon
                    ],
                    dim="time",
                )
                mon_data[seas_name] = self._get_ts_data(cast(xr.Dataset, idx))
            mon_data["total"] = self._get_ts_data(cast(xr.Dataset, mm))
            info[var] = mon_data
        return info

    def get_extent(self, dset: xr.DataArray) -> list[float]:
        """Calculate the lon/lat bounds of the valid data within a dataset."""
        dims = dset.dims[-2:]
        dlon = np.diff(dset[dims[1]].data)[0]
        dlat = np.diff(dset[dims[0]].data)[0]
        return [
            float(dset.X.min().values) - dlon,
            float(dset.X.max().values) + dlon,
            float(dset.Y.min().values) - dlat,
            float(dset.Y.max().values) + dlat,
        ]

    @cached_property
    def general_info(self) -> dict[str, str]:
        """Create a string representation of spatio-temporal averages."""
        info = {}
        dset = self.reference_data, self.projection_data
        for nn, exp in enumerate(self.experiments):
            info[exp] = self._create_information(dset[nn])
        return info

    @cached_property
    def regionborder(self) -> ShapelyFeature:
        """The region border polygon as a cartopy feature."""
        try:
            shape_feature = ShapelyFeature(
                self.shape_data.to_crs(epsg=4326),
                self.transform,
                edgecolor="black",
                facecolor="never",
            )
        except AttributeError:
            shape_feature = cfeature.BORDERS.with_scale("10m")
        return shape_feature

    @staticmethod
    def _get_colormap(
        varname: str, vmin: int, vmax: int
    ) -> tuple[int, int, str]:
        """Get the colormap according to variable name and min, max values."""
        # Water based variables have a diff color Red -> Blue.
        # All others Blue -> Red
        sign = "+-"
        if vmin >= 0:
            sign = "+"
        elif vmax <= 0:
            sign = "-"
        color_lookup = {"+-": "RdBu_r", "-": "Reds_r", "+": "Reds"}
        if sign == "+-":
            mmin = np.max(np.fabs([vmin, vmax]))
            vmin, vmax = -mmin, mmin
        if varname in ("pr", "prw", "cli", "clw", "rh"):
            color_lookup = {"+-": "RdBu", "-": "Blues_r", "+": "Blues"}
        return vmin, vmax, color_lookup[sign]

    def plot_map(self, ensemble_map: xr.DataArray, **kwargs) -> Figure:
        """Create a percentile map plot of a given variable.

        Parameters:
        ===========
        ensemble_map: xr.DataArray
            xarray dataset holding the data that is to be plotted.
        col_wrap: int, optional (default: 3)
            Number of columns in the faceted plot.
        size: int, optional (default: 6)
            Set the size of the plot in inches
        cmap: str, matplotlib.cm, optional
            Overwrite the colormap that would be chosen according to the
            variable
        vmin: int, float, optional
            Set the lower limit of the plot range
        vmax: int, float, optional
            Set the upper limit of the plot range
        add_colorbar: bool, optional (default: true)
            Add a colorbar to the plot
        cbar_label: str, optional
            Overwrite the default colorbar label
        title: str, optional
            The sub title of the plot

        Returns:
        ========
        xarray.plot.plot : xarray DataArray plot object

        """
        times = self.get_time_periods()
        hist = f"{self.experiments['hist']} ({times['hist']})"
        proj = f"{self.experiments['proj']} ({times['proj']})"
        plt.rc("font", size=20)
        title = kwargs.pop(
            "title", (f"Climate change singnal. ({proj} - {hist})")
        )

        varname = ensemble_map.name
        Min = kwargs.pop("vmin", float(ensemble_map.min()))
        Max = kwargs.pop("vmax", float(ensemble_map.max()))
        vmin, vmax, cmap = kwargs.pop(
            "cmap", self._get_colormap(str(varname), Min, Max)
        )
        dims = ensemble_map.dims[-2:]
        lon = ensemble_map[dims[-2]].values.ravel()
        lat = ensemble_map[dims[-1]].values.ravel()
        aspect = len(lon) / len(lat)
        if aspect > 1:
            size = kwargs.get("size", 6)
            aspect = 1 / aspect
        else:
            size = kwargs.get("size", 6)
        # Increae the map extent by 5% but not more than 1 deg
        try:
            # Try getting the plot extent from the mask region
            ext = [
                self.shape_data.bounds["minx"].min(),
                self.shape_data.bounds["maxx"].max(),
                self.shape_data.bounds["miny"].min(),
                self.shape_data.bounds["maxy"].max(),
            ]
        except AttributeError:
            ext = [min(lon), max(lon), min(lat), max(lat)]
        cbar_label = (
            f'{self.attrs[str(varname)]["long_name"]} '
            f'[{self.attrs[str(varname)]["units"]}]'
        )
        plot = xr_plot.plot(
            ensemble_map,
            size=size,
            aspect=aspect,
            transform=self.proj,
            row="Quantile",
            cmap=cmap,
            vmin=vmin,
            vmax=vmax,
            col_wrap=kwargs.get("col_wrap", 3),
            robust=kwargs.get("robust", True),
            add_colorbar=kwargs.get("add_colorbar", True),
            subplot_kws={"projection": self.proj},
            cbar_kwargs={
                "label": kwargs.get("cbar_label", cbar_label),
                "extend": "both",
                "shrink": 1,
                "pad": 1,
                "aspect": 60,
                "orientation": "horizontal",
            },
        )
        for ax in plot.axes.flat:
            for feature in (
                cfeature.COASTLINE.with_scale("10m"),
                self.regionborder,
            ):
                ax.add_feature(feature)
            _ = ax.set_xlim(ext[:2]), ax.set_ylim(ext[-2:])
        plot.fig.subplots_adjust(
            left=0, right=1, hspace=0, wspace=0, top=0.85, bottom=0.2
        )
        plot.fig.suptitle(title, x=0, ha="left", size=17)
        return plot

    def plot_timeseries(self, ts_ref, ts_proj, **kwargs) -> Figure:
        """Create a time series plot of the ensemble.

        Parameters:
        ===========
        size : int, float, optional (default: 8)
            The desired plot size in inches
        ylabel : str, optional
            Overwrite the y-axis label

        Returns:
        ========
        matplotlib.pyplot.figure  : matplotlib time series figure
        """
        times = self.get_time_periods()
        label_hist = f"{self.experiments['hist']} ({times['hist']})"
        label_proj = f"{self.experiments['proj']} ({times['proj']})"
        plt.rc("font", size=20)
        col_blind = sns.color_palette("colorblind", 10)
        sns.set_style("ticks")
        sns.set_palette(col_blind)
        sns.set_context("notebook", rc={"lines.linewidth": 2.5})
        for key, default_value in (
            ("size", 8),
            ("ylabel", None),
            ("title", "Avg. annual cycle"),
            ("xdate", "%b"),
        ):
            kwargs.setdefault(key, default_value)
        fig = plt.figure(figsize=(kwargs["size"], kwargs["size"]))
        if len(self.variable) > 1:
            axs = fig.subplots(len(self.variable), 1, sharex=True).ravel()
        else:
            axs = [fig.add_subplot(111)]
        quants_proj = ts_proj.quantile(QUANTILES, dim="member")
        quants_ref = ts_ref.quantile(QUANTILES, dim="member")
        for nn, var in enumerate(self.variable):
            units = ts_proj[var].attrs.get("units") or self.attrs[var]["units"]
            ylabel = f"{self.attrs[var]['long_name']}" f" [{units}]"
            axs[nn].plot(
                ts_proj.time.values,
                ts_proj[var].median(dim="member").values,
                label=label_proj,
                color=col_blind[1],
            )
            axs[nn].fill_between(
                ts_proj.time.values,
                quants_proj[var].isel(quantile=0).values,
                quants_proj[var].isel(quantile=-1).values,
                alpha=0.3,
                color=col_blind[1],
            )
            axs[nn].plot(
                ts_ref.time.values,
                ts_ref[var].median(dim="member").values,
                label=label_hist,
                color=col_blind[0],
            )
            axs[nn].fill_between(
                ts_ref.time.values,
                quants_ref[var].isel(quantile=0).values,
                quants_ref[var].isel(quantile=-1).values,
                alpha=0.3,
                color=col_blind[0],
            )
            axs[nn].set_ylabel(kwargs["ylabel"] or ylabel)
            axs[nn].set_xlabel(kwargs["title"])
            axs[nn].xaxis.set_major_formatter(
                mdates.DateFormatter(kwargs["xdate"])
            )
            axs[nn].xaxis.set_minor_formatter("")
        axs[0].legend(
            bbox_to_anchor=(0, 1, 1, 0),
            loc="lower left",
            mode="expand",
            ncol=2,
        )
        sns.despine()
        fig.subplots_adjust(
            left=0.1, right=0.98, hspace=0.2, wspace=0.0, top=0.9, bottom=0.1
        )
        fig.suptitle(
            f"{kwargs['title']}: Ensemble median, 0.2 - 0.8 Quantile",
            x=0.1,
            ha="left",
        )
        fig.tight_layout()
        return fig

    def _repr_html_(self, *args, **kwargs) -> str:
        return self._get_html_repr()

    def _get_html_repr(self, header="<style></style>", href=None):
        """HTML representation of the object, nice for notebooks."""
        ext = self.get_extent(
            self.reference_data.isel(time=0, member=0)[self.variable[0]]
        )
        self._ensemble_setup = self._get_ensemble_setup(
            ext,
            header=header,
            href=href,
        )
        return self._ensemble_setup

    @property
    def table(self):
        """HTML representation of spatio-temporal means."""
        return HTML(self._create_html_table())

    def plot(self):
        """Create time series and map plots, as well as additional info."""
        times = self.get_time_periods()
        hist_l = f"{self.experiments['hist']} ({times['hist']})"
        proj_l = f"{self.experiments['proj']} ({times['proj']})"
        if self.write:
            nb_path = Path(self.output_dir) / "ClimateChangeProfile.ipynb"
            link = (
                f"https://jupyterhub.dkrz.de/user/{getuser()}/"
                f"levante-spawner-preset/lab/tree/{nb_path}"
            )
            href = f"""<html><body><h3>To start analysing the data click
                   the linke below</h3><p>
                    <p><a href="{link}"
                    target=_blank>{nb_path}</a>
                    <br>Please keep in mind that this data processing
                    might be <b>memory intense</b>. Hence make sure
                    to allocate a jupyter server setup with enough memory
                    (like 256 GB)</p></body></html>"""
            with (
                Path(self.output_dir)
                / "ClimateChangeProfile_1_general_overview.html"
            ).open("w", encoding="UTF-8") as f:
                f.write(self._get_html_repr(header=None, href=href))
            with (
                Path(self.output_dir)
                / "ClimateChangeProfile_2_season_table.html"
            ).open("w", encoding="UTF-8") as f:
                f.write(self._create_html_table(header=None))
        minmax = {
            v: (
                float(self.ensemble_map[v].min()),
                float(self.ensemble_map[v].max()),
            )
            for v in self.variable
        }
        cmaps = {v: self._get_colormap(v, *minmax[v]) for v in self.variable}
        seasons = ("DJF", "MAM", "JJA", "SON", "annual")
        for seas in seasons:
            if seas == "annual":
                title = f"Annual climate change signal. ({proj_l} - {hist_l})"
                plot_data = self.ensemble_map.mean(dim="season")
            else:
                title = f"{seas} climate change signal. ({proj_l} - {hist_l})"
                plot_data = self.ensemble_map.sel(season=seas)
            for var in self.variable:
                map_plot = self.plot_map(
                    plot_data[var],
                    title=title,
                    vmin=minmax[var][0],
                    vmax=minmax[var][1],
                    cmap=cmaps[var],
                )
                self.map_plots[var][seas] = map_plot
                if self.write:
                    map_plot.fig.savefig(
                        Path(self.output_dir)
                        / f"ClimateChangeProfile_3_{seas}_{var}_map_plot.png",
                        dpi=150,
                        bbox_inches="tight",
                        format="png",
                    )
        ts_proj, ts_ref = self.timeseries_data
        ts_proj = ts_proj.resample(time="7d").mean()
        ts_ref = ts_ref.resample(time="7d").mean()
        self.ts_plot = self.plot_timeseries(ts_ref, ts_proj)
        if self.write:
            self.ts_plot.savefig(
                Path(self.output_dir)
                / "ClimateChangeProfile_4_time_series.png",
                dpi=150,
                bbox_inches="tight",
                format="png",
            )

    @cached_property
    def _geo_data(self) -> Union[gp.GeoDataFrame, None]:
        """Read the geopandas dataframe."""
        shape = None
        if not isinstance(self.shape_file, (str, Path)):
            return None
        try:
            shape = gp.read_file(str(self.shape_file))
        except (DriverError, SchemaError):
            logger.critical("Shape file could not be read")
            return None
        geo_key = [key for key in shape.columns if "geometry" in key.lower()]
        if not geo_key:
            logger.critical("Shape file does not contain 'geometry' key")
            return None
        self._shape_crs = shape.crs
        return shape.to_crs(self.proj)

    @cached_property
    def region_name(self) -> str:
        """Get the name of the region of interest."""
        if self._geo_data is None:
            return ""
        if "GEN" not in self._geo_data.columns:
            return ""
        regions = self._geo_data["GEN"].values.tolist()
        try:
            shape_re = regions[int(self._region_name)]
        except ValueError:
            shape_re = self._region_name
        except (IndexError, TypeError):
            return ""

        return str(shape_re)

    @cached_property
    def shape_data(self):
        """Read the given shape-file."""
        if self._geo_data is None:
            return None
        if not self.region_name:
            return self._geo_data["geometry"]
        keys = [k for k in self._geo_data.columns if k != "geometry"]
        shape_re = []
        for index in self._geo_data.index:
            values = self._geo_data[keys].loc[index]
            if self.region_name in list(values.values):
                break
        shape_re = self._geo_data.loc[self._geo_data.index == index]
        if len(shape_re) > 0:
            return shape_re["geometry"]
        return self._geo_data["geometry"]

    @staticmethod
    def convert_umlaut(inp: str) -> str:
        """Convert all umlaute in a string."""
        inp = inp.replace("ä", "ae")  # A umlaut
        inp = inp.replace("ö", "oe")  # O umlaut
        inp = inp.replace("ü", "ue")  # U umlaut
        inp = inp.replace("Ä", "Ae")  # a umlaut
        inp = inp.replace("Ö", "Oe")  # o umlaut
        inp = inp.replace("Ü", "Ue")  # u umlaut
        inp = inp.replace("ß", "ss")
        return inp

    @cached_property
    def ensemble_map(self):
        """Create map of 3 qantiles for diff of ref. and proj. data."""
        ref_map = Averager.seasonal_mean(self.reference_data)
        proj_map = Averager.seasonal_mean(self.projection_data)
        member = ", ".join(list(ref_map.member.values))
        diff = (proj_map - ref_map).quantile(QUANTILES, dim="member")
        for dv in diff.data_vars:
            diff[dv].attrs = self.reference_data[dv].attrs
        for co in diff.coords:
            diff[dv].attrs = self.reference_data[dv].attrs
        diff.attrs = self.reference_data.attrs
        diff.attrs["experiment"] = (
            f"{self.experiments['proj']}" f" - {self.experiments['hist']}"
        )
        diff.attrs["member"] = member
        return diff.rename({"quantile": "Quantile"})

    @cached_property
    def timeseries_data(self):
        """Create a weighted area mean of the data."""
        dims = self.reference_data[self.variable[0]].dims[-2:]
        proj = self.projection_data[self.variable].mean(dim=dims)
        ref = self.reference_data[self.variable].mean(dim=dims)
        proj.attrs = self.projection_data.attrs
        ref.attrs = self.reference_data.attrs
        proj.attrs["experiment"] = self.experiments["proj"]
        ref.attrs["experiment"] = self.experiments["hist"]
        for dv in self.variable:
            proj[dv].attrs = self.projection_data[dv].attrs
            ref[dv].attrs = self.reference_data[dv].attrs
        return proj, ref

    def _to_cartopy_proj(self, proj: pyproj.crs.crs.CRS) -> crs.Projection:
        code = int(proj.to_string().split(":")[-1])
        try:
            return crs.epsg(code)
        except ValueError:
            pass
        import pyepsg
        from cartopy._epsg import _GLOBE_PARAMS

        projection = pyepsg.get(code)
        proj4_str = projection.as_proj4().strip()
        terms = [term.strip("+").split("=") for term in proj4_str.split(" ")]
        globe_terms = filter(lambda term: term[0] in _GLOBE_PARAMS, terms)
        globe = crs.Globe(
            **{_GLOBE_PARAMS[name]: value for name, value in globe_terms}
        )
        return crs.PlateCarree(globe=globe)

    @staticmethod
    def _get_slices(dset):
        dims = dset.dims[-2:]
        slices = {d: range(len(dset[d])) for d in dset.dims if d not in dims}

        def pair2dict(pairs: list[str], keys: list[str]) -> dict[str, str]:
            return dict(zip(keys, pairs))

        products = it_product(*tuple(slices.values()))
        return map(partial(pair2dict, keys=slices.keys()), products)

    def _get_coords(
        self, data: xr.DataArray, target_proj: crs.Projection
    ) -> xr.Dataset:
        dims = data.dims[-2:]
        X, Y = np.meshgrid(data[dims[1]].values, data[dims[0]].values)
        try:
            grid_mapping = self.shape_data.crs.to_string()
        except AttributeError:
            grid_mapping = "EPSG:4326"  # Default PlateCarree code
        proj_x, proj_y, _ = target_proj.transform_points(self.proj, X, Y).T
        X_new = xr.DataArray(
            proj_x.T,
            coords={dims[1]: data[dims[1]].data, dims[0]: data[dims[0]].data},
            dims=dims,
            attrs={"grid_mapping_name": grid_mapping},
            name="X",
        )
        Y_new = xr.DataArray(
            proj_y.T,
            coords={dims[1]: data[dims[1]].data, dims[0]: data[dims[0]].data},
            dims=dims,
            attrs={"grid_mapping_name": grid_mapping},
            name="Y",
        )
        return xr.Dataset({"X": X_new, "Y": Y_new}).set_coords(("X", "Y"))

    def save_to_netcdf(self):
        """Save the data to netcdf files."""
        proj_ts = self.timeseries_data[0].copy()
        ref_ts = self.timeseries_data[1].copy()
        out_dir = Path(self.output_dir) / "data"
        out_dir.mkdir(exist_ok=True, parents=True)
        member = list(proj_ts["member"].values)
        times = self.get_time_periods()
        t_proj = f'{times["proj"]}'
        t_hist = f'{times["hist"]}'
        nc_files = (
            f'ClimateChangeProfile_timeseries_{self.experiments["proj"]}.nc',
            f'ClimateChangeProfile_timeseries_{self.experiments["hist"]}.nc',
            "ClimateChangeProfile_difference_quantiles_original_grid.nc",
        )
        proj_ts["member"] = xr.DataArray(
            list(range(len(member))),
            name="member",
            dims=("member",),
            attrs={"long_name": "Ensemble member", "units": ""},
        )
        ref_ts["member"] = xr.DataArray(
            list(range(len(member))),
            name="member",
            dims=("member",),
            attrs={"long_name": "Ensemble member", "units": ""},
        )
        proj_ts.attrs["member"] = ", ".join(member)
        ref_ts.attrs["member"] = ", ".join(member)
        ref_ts.attrs["time_period"] = t_hist
        proj_ts.attrs["time_period"] = t_proj
        proj_ts.set_coords("member").to_netcdf(out_dir / nc_files[0])
        ref_ts.set_coords("member").to_netcdf(out_dir / nc_files[1])
        self.ensemble_map.attrs["reference_time_period"] = t_hist
        self.ensemble_map.attrs["projection_time_period"] = t_proj
        self.ensemble_map.to_netcdf(out_dir / nc_files[2])
        with zipfile.ZipFile(
            "01A_Download_ClimateChangeProfile_data.zip", "w"
        ) as zf:
            for ncfile in nc_files:
                zf.write(str(out_dir / ncfile), ncfile)

    def __init__(
        self,
        variables: list[str],
        hist_data: xr.Dataset,
        proj_data: xr.Dataset,
        shape_file: str,
        output_dir: str | None = None,
        write: bool = False,
    ):
        """Create multi-model ensemble from netCDF datasets."""
        super().__init__(variables, hist_data, proj_data)

        self.shape_file = shape_file
        self.reference_data.attrs["region_name"] = self.region_name
        self.projection_data.attrs["region_name"] = self.region_name
        self.output_dir = Path(output_dir or ".").expanduser().absolute()
        self.map_plots: Dict[str, Dict[str, Any]] = {
            v: {} for v in self.variable
        }
        logger.info("Preparing ensemble ...")
        self.write = write
        self._mask = hist_data.mask.data
        self.reference_data.attrs["member"] = ", ".join(
            hist_data.member.values
        )
        self.projection_data.attrs["member"] = ", ".join(
            proj_data.member.values
        )
        for dv in self.variable:
            target_unit = (
                self.units[dv] or self.reference_data[dv].attrs["units"]
            )
            self.reference_data[dv] = UnitConverter.convert(
                self.reference_data[dv], target_unit
            )
            self.projection_data[dv] = UnitConverter.convert(
                self.projection_data[dv], target_unit
            )
            if (target_unit or "").lower() == "degc":
                self._units[dv] = target_unit = "°C"
            if (target_unit or "").lower() == "mm/h":
                self._units[dv] = target_unit = "mm/d"
                self.reference_data[dv].data *= 24
                self.projection_data[dv].data *= 24
            self.attrs[dv]["units"] = target_unit
            self.reference_data[dv].attrs["units"] = target_unit
            self.projection_data[dv].attrs["units"] = target_unit
        logger.info("Preparing ensemble ... done")
        if self.write:
            logger.info("Saving netcdf data ...")
            self.save_to_netcdf()
            logger.info("Saving netcdf data ... done")

# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause

"""Module that facilitates plotting data."""
from __future__ import annotations

from abc import abstractmethod
from functools import cached_property
from typing import Any, Dict, List

import humanize
import numpy as np
import pandas as pd
import xarray as xr
from numpy.typing import NDArray

HEADER = """<head>
    <style>
        ul li {{
    color: black;
    list-style-type: none;
    margin-left: .1em;
        }}
    ul li:before {{
        content: '■	';
        color: tomato;
        list-style-type: square;
        padding-right: 0.5em;
    }}
    h1{{color: tomato;}}
    h2{{color: tomato;}}
        table {{
            font-family: arial, sans-serif;
            border-collapse: collapse;
           width: 100%;
        }}
        td, th {{
            border: 0px solid #ff5722;
            text-align: right;
            padding: 8px;
        }}
        tr:nth-child(even) {{
            background-color: #dddddd;
        }}
    </style>
    </head>
"""


class Information:
    def __init__(self, dataset: xr.Dataset, variables: list[str]):
        dims = dataset[variables[0]].dims[-2:]
        self._dataset = dataset.mean(dim=dims).resample(time="m")
        self.data_vars = variables

    @property
    def seas(self):
        """Name of seasons"""
        return {
            "DJF": ("Dec", "Jan", "Feb"),
            "MAM": ("Mar", "Apr", "May"),
            "JJA": ("Jun", "Jul", "Aug"),
            "SON": ("Sep", "Oct", "Nov"),
            "Whole Year": (
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
            ),
        }

    def _create_dataset(self, attr: str) -> Dict[str, List]:
        months = [
            tt.strftime("%b")
            for tt in pd.DatetimeIndex(self._dataset.mean().time.values)
        ]
        seas_data: Dict[str, List] = {dv: [] for dv in self.data_vars}
        dataset = list(self._dataset)
        for seas_name, mon in self.seas.items():
            dset = xr.concat(
                [
                    dataset[i][-1]
                    for i in range(len(months))
                    if months[i] in mon
                ],
                dim="time",
            )
            for v in self.data_vars:
                mean_data = float(
                    getattr(dset[v], attr)(dim="time")
                    .median(dim="member")
                    .values
                )
                std_data = float(
                    getattr(dset[v], attr)(dim="time").std(dim="member").values
                )
                seas_data[v].append(np.array([mean_data, std_data]))
        return seas_data

    @cached_property
    def min(self) -> pd.DataFrame:
        """Calculate seasonal minimum values of the dataset."""
        return pd.DataFrame(
            self._create_dataset("min"), index=list(self.seas.keys())
        )

    @cached_property
    def mean(self) -> pd.DataFrame:
        """Calculate seasonal mean values of the dataset."""
        return pd.DataFrame(
            self._create_dataset("mean"), index=list(self.seas.keys())
        )

    @cached_property
    def max(self) -> pd.DataFrame:
        """Calculate seasonal maximum values of the dataset."""
        return pd.DataFrame(
            self._create_dataset("max"), index=list(self.seas.keys())
        )

    @staticmethod
    def format(inp: pd.DataFrame) -> pd.DataFrame:
        """Make dataset human readable."""
        output: Dict[str, List[str]] = {v: [] for v in inp.columns}
        for v in inp.columns:
            for mean_data, std_data in inp[v].values:
                fabs, sign = np.fabs(mean_data), str(np.sign(mean_data)).strip(
                    "1.0"
                )
                value = (
                    sign
                    + humanize.scientific(fabs, 1)
                    + " ± "
                    + humanize.scientific(std_data, 1)
                )
                output[v].append(value.replace("--", "-"))
        return pd.DataFrame(output, index=inp.index)


class AuxInfo:
    """Helper class to define colormaps and variable units."""

    _cmap = {
        "tas": "RdYlBu_r",
        "ta": "RdYlBu_r",
        "pfull": "RdYlBu_r",
        "dew": "RdYlBu_r",
        "cllvi": "BuPu",
        "clivi": "BuPu",
        "cli": "BuPu",
        "clw": "BuPu",
        "rh": "BuPu",
        "qsvi": "BuPu",
        "qgvi": "BuPu",
        "qrvi": "BuPu",
        "clt": "Greys_r",
        "cl_low": "BuPu",
        "hfls": "RdYlBu_r",
        "hfss": "RdYlBu_r",
        "rlut": "RdYlBu_r",
        "rsut": "RdYlBu_r",
        "rsds": "RdYlBu_r",
        "rlds": "RdYlBu_r",
        "rsus": "RdYlBu_r",
        "rlus": "RdYlBu_r",
        "nswrf_t": "RdYlBu_r",
        "nlwrf_t": "RdYlBu_r",
        "nswrf_s": "RdYlBu_r",
        "nlwrf_s": "RdYlBu_r",
        "net_surf_energy": "RdYlBu_r",
        "rsdt": "RdYlBu_r",
        "net_toa": "RdYlBu_r",
        "net_surf": "RdYlBu_r",
        "prw": "BuPu",
        "pr": "Blues",
        "ts": "RdYlBu_r",
        "vas": "RdYlBu_r",
        "va": "RdYlBu_r",
        "uas": "RdYlBu_r",
        "ua": "RdYlBu_r",
        "wap": "RdYlBu_r",
        "w": "RdYlBu_r",
        "qs": "BuPu",
        "hus": "BuPu",
        "uas_ice": "RdYlBu_r",
        "vas_ice": "RdYlBu_r",
        "uas_lnd": "RdYlBu_r",
        "vas_lnd": "RdYlBu_r",
        "uas_wtr": "RdYlBu_r",
        "vas_wtr": "RdYlBu_r",
        "wsp": "RdYlBu_r",
        "ttrend": "RdYlBu_r",
        "to": "RdYlBu_r",
        "so": "BuPu",
        "u": "RdYlBu_r",
        "v": "RdYlBu_r",
        "mlotst": "BuPu",
        "atmos_fluxes_FrshFlux_Evaporation": "RdYlBu_r",
    }

    _units = {
        "tas": "degC",
        "ta": "degC",
        "pfull": "Pa",
        "dew": "degC",
        "cllvi": "kg/m^2",
        "clivi": "kg/m^2",
        "cli": "kg/kg",
        "clw": "kg/kg",
        "rh": " ",
        "qsvi": "kg/m^2",
        "qgvi": "kg/m^2",
        "qrvi": "kg/m^2",
        "clt": "%",
        "cl_low": "kg/kg",
        "hfls": "W/m^2",
        "hfss": "W/m^2",
        "rlut": "W/m^2",
        "rsut": "W/m^2",
        "rsds": "W/m^2",
        "rlds": "W/m^2",
        "rsus": "W/m^2",
        "rlus": "W/m^2",
        "nswrf_t": "W/m^2",
        "nlwrf_t": "W/m^2",
        "nswrf_s": "W/m^2",
        "nlwrf_s": "W/m^2",
        "net_surf_energy": "W/m^2",
        "rsdt": "W/m^2",
        "net_toa": "W/m^2",
        "net_surf": "W/m^2",
        "prw": "kg/m^2",
        "pr": "mm/h",
        "ts": "degC",
        "vas": "m/s",
        "va": "m/s",
        "uas": "m/s",
        "ua": "m/s",
        "wap": "Pa/s",
        "w": "m/s",
        "qs": "kg/kg",
        "hus": "kg/kg",
        "uas_ice": "m/s",
        "vas_ice": "m/s",
        "uas_lnd": "m/s",
        "vas_lnd": "m/s",
        "uas_wtr": "m/s",
        "vas_wtr": "m/s",
        "wsp": "m/s",
        "ttrend": "degC",
        "to": "degC",
        "so": "psu",
        "u": "m/s",
        "v": "m/s",
        "mlotst": "m",
        "atmos_fluxes_FrshFlux_Evaporation": "m/s",
    }
    _fallback = "RdYlBu_r"

    @abstractmethod
    def __init__(
        self,
        variables: list[str],
        hist_data: xr.Dataset,
        proj_data: xr.Dataset,
    ):
        """Create abstract method that is only be inheritetd."""
        self._variables = variables
        self.variable = variables
        self.experiments = {
            "hist": hist_data.attrs["experiment"],
            "proj": proj_data.attrs["experiment"],
        }
        self._region_name = hist_data.attrs.pop("region_name", "")
        self.start = hist_data["start"].values[0]
        self.end = hist_data["end"].values[0]
        self.global_warming_level = proj_data.attrs["warming_level"]
        self.projects = proj_data.attrs["projects"].split(",")
        self.product = proj_data.attrs["product"].split(",")
        self.projection_data = proj_data
        self.reference_data = hist_data
        self.attrs: Dict[str, Dict[str, str]] = {}
        for var in self._variables:
            self.attrs[var] = {}
            for key in ("units", "long_name"):
                self.attrs[var][key] = hist_data[var].attrs[key]

    @property
    def colormaps(self) -> dict[str, str]:
        """Look for the correct color maps."""
        return {v: self._cmap.get(v, self._fallback) for v in self._variables}

    @property
    def linecolors(self) -> dict[str, str]:
        """Set the line color, according to the color map."""
        dark = ("Greys_r",)
        colors = {}
        for v in self._variables:
            colors[v] = "w"
            if self._cmap.get(v, self._fallback) in dark:
                colors[v] = "w"
        return colors

    @property
    def units(self) -> dict[str, str]:
        """Look for the correct units."""
        return {v: self._units.get(v, "") for v in self._variables}

    def get_time_periods(self) -> dict[str, str]:
        """Get the considered time periods."""

        return {
            "hist": f"{self.start} - {self.end}",
            "proj": f"Warming lev. {self.global_warming_level}",
        }

    def _get_ensemble_setup(
        self, extent: NDArray[Any], header: str = "", href: str = ""
    ) -> str:
        """Create a html interpretation of the ensemble definition."""
        times = self.get_time_periods()
        region_name = self._region_name or ""
        extent = np.round(extent, 2)
        warning_msg = []
        dt = np.fabs(self.end - self.start)
        if dt < 20:
            warning_msg.append(
                (
                    f"Only {dt} years were chosen to calculate averages"
                    " over the the historical period, your results "
                    "might be misleading."
                )
            )
        start_str = f"""<h1>Experiment Setup:</h1>
        <ul>
            <li><b>Experiments</b>:
            {self.experiments['hist']} ({times['hist']})
            {self.experiments['proj']} ({times['proj']})
            <li><b>Project(s)</b>: {', '.join(self.projects)}
            <li><b>Product(s)</b>: {', '.join(self.product)}
            <li><b>Region</b>:
             {region_name}
              (lon: {extent[0]} - {extent[1]}, lat: {extent[-2]} - {extent[-1]})
        </ul>
        """

        hist_member, proj_member = [], []
        for member in map(str, self.projection_data["member"].values):
            start = self.projection_data["start"].sel(member=member).values
            end = self.projection_data["end"].sel(member=member).values
            proj_member.append(f"{member} ({int(start)} - {int(end)})")
        for member in map(str, self.reference_data["member"].values):
            start = self.reference_data["start"].sel(member=member).values
            end = self.reference_data["end"].sel(member=member).values
            hist_member.append(f"{member} ({int(start)} - {int(end)})")

        if len(hist_member) < 10 or len(proj_member) < 10:
            warning_msg.append(
                ("Your small ensemble size might lead to misleading results")
            )
        if warning_msg:
            start_str += "<h3>Warning</h3>"
            start_str += "<p>There might be issues with your setup:</p><ul>"
            start_str += " ".join([f"<li>{w}" for w in warning_msg])
            start_str += "</ul>"
        start_str += f"""<h2>Projection exp.
        ({self.experiments["proj"]}) setup
         ({self.projection_data["member"].shape[0]} member)</h2><ul>"""
        start_str += " ".join([f"<li>{d}" for d in proj_member])
        start_str += f"""</ul>
        <h2>Reference exp. ({self.experiments["hist"]})
         setup ({self.reference_data["member"].shape[0]} member)</h2><ul>"""
        start_str += " ".join([f"<li>{d}" for d in hist_member])
        start_str += "</ul>"
        header = header or HEADER
        href = href or ""
        return f"""

        <html>
        {header}
        <body>
        {href}
    <div>
    {start_str}
    </div>
    </body>
    </html>
        """

    def _create_html_table(self, header: str = "<style></style>") -> str:
        html = "<html>{header}<body><div>{table}</div></body></html>"
        html_str = ["<h1>Climate Change Profiles by Season</h1>"]
        table_overview = self._get_table()
        for variable, table in table_overview.items():
            html_str.append(
                f"<h3>{self.attrs[variable]['long_name']}"
                f" [{self.attrs[variable]['units']}]</h3>"
            )
            html_str.append(table)
        html_str.append("<br><p>")
        html_str.append(
            "<b>DJF</b>: Dec./Jan./Feb. <b>MAM</b>: Mar./Apr./May. "
            "<b>JJA</b>: Jun./Jul./Aug. <b>SON</b>: Sep./Oct./Nov.</p>"
        )
        return html.format(header=header or HEADER, table="\n".join(html_str))

    def _get_table(self, attr: str = "mean") -> dict[str, str]:
        """Create html table representation of the data."""

        ref_info = Information(self.reference_data, self.variable)
        proj_info = Information(self.projection_data, self.variable)
        table = {}
        for v in ref_info.data_vars:
            data = {}
            diff = getattr(proj_info, attr) - getattr(ref_info, attr)
            diff_mean = {}
            for i in range(proj_info.mean[v].values.size):
                mean1, std1 = getattr(proj_info, attr)[v].values[i]
                mean2, std2 = getattr(ref_info, attr)[v].values[i]
                mean = mean1 - mean2
                std = np.sqrt((std1**2 + std2**2) / 2)
                diff_mean[proj_info.mean.index[i]] = (mean, std)
            diff_mean = pd.DataFrame({v: diff_mean})
            data = {
                self.experiments["proj"]: Information.format(
                    getattr(proj_info, attr)
                )[v],
                self.experiments["hist"]: Information.format(
                    getattr(ref_info, attr)
                )[v],
                "difference": Information.format(diff_mean)[v].values,
            }
            table[v] = pd.DataFrame(data, index=diff.index).to_html()
        return table

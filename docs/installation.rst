.. SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _installation:

Installation
============

.. warning::

   This page has been automatically generated as has not yet been reviewed by the
   authors of climate-change-profile!

To install the `climate-change-profile` package, we recommend that
you install it directly from `the source code repository on Gitlab`_ via::

    pip install git+https://gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile.git

The latter should however only be done if you want to access the development
versions.

.. _the source code repository on Gitlab: https://gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile


.. _install-develop:

Installation for development
----------------------------
Please head over to our :ref:`contributing guide <contributing>` for
installation instruction for development.

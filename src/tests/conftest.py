# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: CC0-1.0

import configparser

import pytest
import toml


def update_evaluation_system_config():
    evaluation_system_config_file = "plugin_env/freva/evaluation_system.conf"
    config = configparser.ConfigParser()
    config.read(evaluation_system_config_file)
    config.sections()
    config["evaluation_system"]["db.host"] = "127.0.0.1"
    config["evaluation_system"]["db.user"] = "freva"
    config["evaluation_system"]["db.passwd"] = "T3st"
    config["evaluation_system"]["db.db"] = "freva"
    config["evaluation_system"]["db.port"] = "3306"
    config["evaluation_system"]["solr.host"] = "localhost"
    with open(evaluation_system_config_file, "w") as f:
        config.write(f)


def update_drs_config():
    drs_config_file = "plugin_env/freva/drs_config.toml"
    config = toml.load(drs_config_file)
    # TODO: Change the following lines to update the configuration in your plugin
    config["cordex"] = {
        "root_dir": "/home/freva/work/kd0956/CORDEX/data",
        "drs_format": "cordex",
    }
    config["cordex.defaults"] = {"project": "cordex"}
    config["cmip5"] = {
        "root_dir": "/home/freva/work/kd0956/CMIP5/data",
        "drs_format": "cmip5",
    }
    with open(drs_config_file, "w") as file:
        toml.dump(config, file)


@pytest.fixture(scope="session", autouse=True)
def update_freva_configs():
    try:
        update_evaluation_system_config()
        update_drs_config()
        print("Configuration updated successfully.")
    except Exception as e:
        raise e

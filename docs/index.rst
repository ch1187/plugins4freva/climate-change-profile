.. SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
..
.. SPDX-License-Identifier: CC-BY-4.0

.. climate-change-profile documentation master file
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to climate-change-profile's documentation!
==================================================

|CI|
|Code coverage|
|Functional|
|Code style: black|
|Imports: isort|
|PEP8|
|Checked with mypy|
|REUSE status|

.. rubric:: This freva plugin calculates climate change profiles for given climage change scenarios.

.. warning::

    This page has been automatically generated as has not yet been reviewed by
    the authors of climate-change-profile!
    Stay tuned for updates and discuss with us at
    https://gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile


The user can choose a multi-model ensemble for a given `product`.

Currently  only one out of three different `products` can be selected.
The `experiment` _historical_ is fixed as reference experiment. Meaning the
user can only choose the climate change scenario. Another important input is
the time periods. Periods are fixed to a 30 year length. The start point
of the projection time period is depending on the global warming level
the users sets.

To specify the considered region either a user defined shape file or a predefined
region can be selcted.

In the background a jupyter-notebook is parameterised and executed.
After the execution a link to the parameterised jupyter notebook on the jupyter
hub is created. This gives the user the opportunity to jump right back into
the notebook re-create the ensemble and continue a more in-depth analysis.

The code itself combines a multi-model ensemble of different realisations
and model member. The code tries to combine data with different calendars
(360 days, no leap, all leap etc) into a single multi-year annual cycle.
This is done for both experiments (_historical_ and _climate change_).
If something goes wrong while opening the netcdf files
- that is if data is corrupted - the code can either be set to drop model member
with corrupted files and continue or quit the execution.

Once the muli-year ensemble is created difference maps of
3 Ensemble Quantiles are calculated, plotted and the data is saved to file.
Also a multi-year annual cycle time series, displaying the ensemble spread and
median for the considered region is displayed and the data is saved to file.
The maps are saved in thier native grid definition. Since, especially for
rotated ploe coordinates, this can be confusing to users the additional data that
underwent an affine transformation - in accordance to the projection of the
shapefile - will be applied. If no shape file was chosen as a mask,
a Platte Carree transformation is applied.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   api
   contributing


How to cite this software
-------------------------

.. card:: Please do cite this software!

   .. tab-set::

      .. tab-item:: APA

         .. citation-info::
            :format: apalike

      .. tab-item:: BibTex

         .. citation-info::
            :format: bibtex

      .. tab-item:: RIS

         .. citation-info::
            :format: ris

      .. tab-item:: Endnote

         .. citation-info::
            :format: endnote

      .. tab-item:: CFF

         .. citation-info::
            :format: cff


License information
-------------------

The source code of climate-change-profile is licensed under
BSD-3-Clause.

If not stated otherwise, the contents of this documentation is licensed under
CC-BY-4.0.




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. |CI| image:: https://gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile/badges/main/pipeline.svg
   :target: https://gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile/-/pipelines?page=1&scope=all&ref=main
.. |Code coverage| image:: https://gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile/badges/main/coverage.svg
   :target: https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climate-change-profile/htmlcov/
.. |Functional| image:: https://img.shields.io/badge/Functional-Test-brown.svg
   :target: https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climate-change-profile/func_test/
.. |Code style: black| image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
.. |Imports: isort| image:: https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336
   :target: https://pycqa.github.io/isort/
.. |PEP8| image:: https://img.shields.io/badge/code%20style-pep8-orange.svg
   :target: https://www.python.org/dev/peps/pep-0008/
.. |Checked with mypy| image:: http://www.mypy-lang.org/static/mypy_badge.svg
   :target: http://mypy-lang.org/
.. |REUSE status| image:: https://api.reuse.software/badge/gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile
   :target: https://api.reuse.software/info/gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile

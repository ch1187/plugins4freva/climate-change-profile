<!--
SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum

SPDX-License-Identifier: CC-BY-4.0
-->

# Creating Climate Change Profiles

[![CI](https://gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile/badges/main/pipeline.svg)](https://gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile/-/pipelines?page=1&scope=all&ref=main)
[![Code coverage](https://gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile/badges/main/coverage.svg)](https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climate-change-profile/htmlcov/)
[![Docs](https://img.shields.io/badge/docs-Passed-green.svg)](https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climate-change-profile/docs/)
[![Functional](https://img.shields.io/badge/Functional-Test-brown.svg)](https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climate-change-profile/func_test/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/)
[![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)
[![REUSE status](https://api.reuse.software/badge/gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile)](https://api.reuse.software/info/gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile)


This freva plugin calculates climate change profiles for given climate
change scenarios.

The user can choose a multi-model ensemble for a given `product`.

Currently  only one out of three different `products` can be selected.
The `experiment` _historical_ is fixed as reference experiment. Meaning the
user can only choose the climate change scenario. Another important input is
the time periods. Periods are fixed to a 30 year length. The start point
of the projection time period is depending on the global warming level
the users sets.

To specify the considered region, select either a user-defined shape file or
a pre-defined region from the dropdown menu.

In the background a jupyter-notebook is parameterised and executed.
After the execution a link to the parameterised jupyter notebook on the jupyter
hub is created. This gives the user the opportunity to jump right back into
the notebook re-create the ensemble and continue a more in-depth analysis.

The code itself combines a multi-model ensemble of different realisations
and model member. The code tries to combine data with different calendars
(360 days, no leap, all leap etc) into a single multi-year annual cycle.
This is done for both experiments (_historical_ and _climate change_).
If something goes wrong while opening the netcdf files
- that is if data is corrupted - the code can either be set to drop model member
with corrupted files and continue or quit the execution.

Once the muli-year ensemble is created difference maps of
3 Ensemble Quantiles are calculated, plotted and the data is saved to file.
Also a multi-year annual cycle time series, displaying the ensemble spread and
median for the considered region is displayed and the data is saved to file.
The maps are saved in thier native grid definition. Since, especially for
rotated ploe coordinates, this can be confusing to users the additional data that
underwent an affine transformation - in accordance to the projection of the
shapefile - will be applied. If no shape file was chosen as a mask,
a Platte Carree transformation is applied.


## Connection and Module Loading

To connect to the server and load the necessary modules for the project, use the following commands:

```bash
ssh <user-id>@levante.dkrz.de
module load clint <project-module>
```

whcih in RegKlim project, `<project-module>` is `regiklim-ces`

## Integrating Creating Climate Change Profiles with the Plugin

To integrate Creating Climate Change Profiles into your Freva instance, follow these steps:

1. **Clone the Plugin Repository:**

    Navigate to the appropriate directory and clone the repository using the following commands:

    ```bash
    cd <project-dir>/<username>/plugins/
    git clone https://gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile.git
    ```

    in RegiKlim, `<project-dir>` is set to `/work/ch1187/regiklim-work/`

2. **Install Prerequisites:**

    After cloning the repository, install the required packages:

    ```bash
    cd climate-change-profile
    make dep-install
    conda activate plugin_env
    make cartopy
    pip install .
    ```

    or simply the following conda command:

    ```bash
    cd climate-change-profile
    conda env create --prefix ./plugin_env -f climate-change-profile-env.yml
    conda activate plugin_env
    make cartopy
    pip install .
    ```

3. **Add Your Scientific Scripts to the Plugin**

    With the framework for your plugin in place, you can now add your scientific code. Suppose you have added a scientific script at `src/ccp/scientific_script.py`. To properly integrate this script, follow these steps:

    ```bash
    cd climate-change-profile
    reuse annotate --year 2024 --license CC0-1.0 --copyright "Deutsche Klimarechenzentrum" src/ccp/scientific_script.py
    ```

    Repeat this process for each new script you generate.

4. **Configure the Freva Instance:**

    Use the following export command to make the plugin available in your Freva instance:

    ```bash
    export EVALUATION_SYSTEM_PLUGINS=<project-dir>/<username>/plugins/climate-change-profile,climate-change-profile-wrapper-api
    ```

    For example, in RegiKlim, `<project-dir>` is set to `/work/ch1187/regiklim-work/`. You can adjust this path based on the specific
    project you want to plug it into.


Now, the plugin is ready to be used in your Freva instance.


To use this in a development setup, clone the [source code][source code] from
gitlab, start the development server and make your changes::

```bash
git clone https://gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile.git
cd climate-change-profile
conda env create --prefix ./plugin_env -f climate-change-profile-env.yml
conda activate plugin_env
```

More detailed installation instructions my be found in the [docs][docs].


[source code]: https://gitlab.dkrz.de/ch1187/plugins4freva/climate-change-profile
[docs]: https://climate-change-profile.readthedocs.io/en/latest/installation.html


## Contribution in this plugin


## Technical note

This package has been generated from the template
[Freva Plugin Template](https://gitlab.dkrz.de/ch1187/plugins4freva/freva-plugin-template).

See the template repository for instructions on how to update the skeleton for
this package.


## License information

Copyright © 2024 Deutsche Klimarechenzentrum



Code files in this repository are licensed under the
BSD-3-Clause, if not stated otherwise in the file.

Documentation files in this repository are licensed under CC-BY-4.0, if not stated otherwise in the file.


Supplementary and configuration files in this repository are licensed
under CC0-1.0, if not stated otherwise
in the file.

Please check the header of the individual files for more detailed
information.


###  `pre-commit` usage

To use the pre-commit setup, follow these steps:

1. Activate Hooks:
   - run the following command to set up the git hooks:
     ```bash
     pre-commit install
     ```

2. Automatic Checks:
   - Now, each time you attempt to commit changes, the pre-commit hooks will automatically run to ensure your files adhere to the specified standards.

3. Manual Execution:
   - If you want to manually run the hooks on all files, use:
     ```bash
     pre-commit run --all-files
     ```

### License management

License management is handled with [``reuse``](https://reuse.readthedocs.io/).
If you have any questions on this, please have a look into the
[contributing guide][contributing] or contact the maintainers of
`climate-change-profile`.
[contributing]: https://climate-change-profile.readthedocs.io/en/latest/contributing.html
